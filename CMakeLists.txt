cmake_minimum_required (VERSION 3.1)
project(test-travis-ci)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)

include(CTest)
enable_testing()

include_directories(src)
add_executable(hello src/hello.cxx)
add_test(hello hello)
