/** @file libket/QConfig.h

    @brief LibKet configuration

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QCONFIG_H
#define QCONFIG_H

#ifdef __cplusplus
#include <complex>
#endif

// Define the LibKet version number
#define LIBKET_VERSION  "2020.05-233"   
#define LIBKET_MAJOR    2020
#define LIBKET_MINOR    05
#define LIBKET_REVISION 233

#define LIBKET_BINARY   "/Users/mmoller/codes/LibKet/build-gcc9/"

#ifdef __cplusplus

/// Define index type
#define index_t long long
  
/// Define real type
#define real_t double

/// Define complex data type  
#define complex_t std::complex<real_t>

#endif

#endif // QCONFIG_H
