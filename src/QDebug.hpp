/** @file libket/QDebug.hpp

@brief LibKet debugging and messaging system

@copyright This file is part of the LibKet library

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#pragma once
#ifndef QDEBUG_HPP
#define QDEBUG_HPP

#include <iostream>

namespace LibKet {

/**
   @brief LibKet standard logging messages

   QInfo is ment to be the standard output stream, like for the
   output of the executables. In general, the library should not
   write to QInfo.
*/
#define QInfo std::cout

/**
   @brief LibKet warning logging messages

   QWarn is for warnings, eg, for missing functionality or problem
   in the input.

   Note that QWarn cannot be given as a parameter to another
   function.
*/
#define QWarn std::cout << "Warning: "

/**
   @brief LibKet debugging logging messages

   QDebug and QDebugVar(.) are for debugging messages and are
   enabled in debug mode only.

   Note that QDebug cannot be given as a parameter to another
   function.
*/
#ifndef NDEBUG

#define QDebug std::cout << "LIBKET_DEBUG: "

#define QDebugVar(variable)                                                    \
  QDebug << (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)   \
         << ":" << __LINE__ << ", " #variable ": \n"                           \
         << (variable) << "\n"
#define QDebugIf(cond, variable)                                               \
  if (cond)                                                                    \
  QDebug << "[ " #cond " ] -- "                                                \
         << (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)   \
         << ":" << __LINE__ << ", " #variable ": \n"                           \
         << (variable) << "\n"
#else
#define QDebug                                                                 \
  if (0)                                                                       \
  std::cout
#define QDebugVar(variable)
#define QDebugIf(cond, variable)
#endif

/**
 *  Runtime assertions which display a message
 *
 */
#ifndef NDEBUG
#define LIBKET_ASSERT(cond, message)                                           \
  do                                                                           \
    if (!(cond)) {                                                             \
      std::stringstream _m_;                                                   \
      _m_ << "LIBKET_ASSERT `" << #cond << "` " << message << "\n"             \
          << __FILE__ << ", line " << __LINE__ << " (" << __FUNCTION__ << ")"; \
      throw std::logic_error(_m_.str());                                       \
    }                                                                          \
  while (false)
#else
#define LIBKET_ASSERT(condition, message)
#endif

/**
 *  Runtime check and display error message. This command is the same as
 *  LIBKET_ASSERT but it is executed in release builds as well.
 *
 */
#define LIBKET_ENSURE(cond, message)                                           \
  do                                                                           \
    if (!(cond)) {                                                             \
      std::stringstream _m_;                                                   \
      _m_ << "LIBKET_ENSURE `" << #cond << "` " << message << "\n"             \
          << __FILE__ << ", line " << __LINE__ << " (" << __FUNCTION__ << ")"; \
      throw std::runtime_error(_m_.str());                                     \
    }                                                                          \
  while (false)

/**
 *  Denote a variable as unused, used to silence warnings in release
 *  mode builds.
 *
 */
#define LIBKET_UNUSED(x) static_cast<void>(x)

/**
 *  Runtime error message
 *
 */
#define LIBKET_ERROR(message)                                                  \
  do {                                                                         \
    std::stringstream _m_;                                                     \
    _m_ << "LIBKET_ERROR " << message << "\n"                                  \
        << __FILE__ << ", line " << __LINE__ << " (" << __FUNCTION__ << ")";   \
    throw std::runtime_error(_m_.str());                                       \
  } while (false)

/**
 * Extension of the std::getenv function that returns a pre-defined
 * default value if the environment variable is not found. If no
 * default value is specified then the empty char is returned.
 *
 */
const char*
getenv(const char* env_var, const char* default_value = "\0")
{
  char* env = std::getenv(env_var);
  if (env)
    return env;
  else {
    return default_value;
  }
}

} // namespace LibKet

#endif // QDEBUG_HPP
