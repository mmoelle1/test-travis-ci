/** @file libket/circuits/QCircuit_Phase_Est.hpp

    @brief LibKet phase estimation circuit class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers
*/

#pragma once
#ifndef QCIRCUIT_PHASE_EST_HPP
#define QCIRCUIT_PHASE_EST_HPP

#include <QFilter.hpp>
#include <QUtils.hpp>
#include <circuits/QCircuits.hpp>
#include <gates/QGates.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace circuits {
/**
@brief LibKet phase estimation circuit class

The LibKet phase estimation circuit class implements a circuit which estimates
the phase of a given unitary
*/
template<typename _Ugate, typename _tol = QConst_t(0.0)>
class QCircuit_Phase_Est : public QCircuit
{
private:
public:
  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& to, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

  /// Apply function - used for all backends
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           QBackendType _qbackend>
  inline static QData<_qubits, _qbackend>& apply(
    QData<_qubits, _qbackend>& data) noexcept
  {
    auto expr = qft<>(_filter0);
    return expr(data);
  }
};

/**

@brief LibKet phase estimation circuit creator


This overload of the LibKet::circuits:phase_est() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::phase_est(expr);
\endcode
*/

template<typename _tol = QConst_t(0.0)>
inline constexpr auto
phase_est() noexcept
{
  return TernaryQGate<filters::QFilter,
                      filters::QFilter,
                      QCircuit_Phase_Est<_tol>>(filters::QFilter{},
                                                filters::QFilter{});
}

/**

@brief LibKet phase estimation circuit creator


This overload of the LibKet::circuits:phase_est() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::phase_est(expr);
\endcode
*/

template<typename _Ugate,
         typename _tol = QConst_t(0.0),
         typename _expr0,
         typename _expr1>
inline constexpr auto
phase_est(_Ugate, const _expr0& expr0, const _expr1& expr1) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      QCircuit_Phase_Est<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}
/**
@brief LibKet phase estimation circuit creator

This overload of the LibKet::circuits::phase_est() function accepts an
expression as universal reference
*/
template<typename _Ugate,
         typename _tol = QConst_t(0.0),
         typename _expr0,
         typename _expr1>
inline constexpr auto
phase_est(_Ugate, const _expr0& expr0, _expr1&& expr1) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      QCircuit_Phase_Est<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

template<typename _Ugate,
         typename _tol = QConst_t(0.0),
         typename _expr0,
         typename _expr1>
inline constexpr auto
phase_est(_Ugate, _expr0&& expr0, const _expr1& expr1) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      QCircuit_Phase_Est<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

template<typename _Ugate,
         typename _tol = QConst_t(0.0),
         typename _expr0,
         typename _expr1>
inline constexpr auto
phase_est(_Ugate, _expr0&& expr0, _expr1&& expr1) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      QCircuit_Phase_Est<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet phase estimation circuit creator

Function alias for LibKet::circuits::phase_est
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
PHASE_EST(Args&&... args)
{
  return phase_est<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet phase estimation circuit creator

Function alias for LibKet::circuits::phase_est
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
Phase_Est(Args&&... args)
{
  return phase_est<_tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Phase_Est<_Ugate, _tol>::operator()(const T0& t0,
                                             const T1& t1) const noexcept
{
  return phase_est<_Ugate, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}
/// Operator() - by universal reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Phase_Est<_Ugate, _tol>::operator()(T0&& t0, T1&& t1) const noexcept
{
  return arb_ctrl<_Ugate, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Phase_Est<_Ugate, _tol>::operator()(T0&& t0,
                                             const T1& t1) const noexcept
{
  return arb_ctrl<_Ugate, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Phase_Est<_Ugate, _tol>::operator()(const T0& t0,
                                             T1&& t1) const noexcept
{
  return arb_ctrl<_Ugate, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief libket show gate type - specialization for phase estimation circuit
   objects
*/
template<std::size_t level = 1, typename _Ugate, typename _tol = QConst_t(0.0)>
inline static auto
show(const QCircuit_Phase_Est<_Ugate, _tol>& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "PHASE_EST\n";

  return circuit;
}

} // namespace circuits
} // namespace libket

#endif // QCIRCUIT_PHASE_EST
