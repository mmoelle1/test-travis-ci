/** @file libket/circuits/QCircuit.hpp

    @brief LibKet quantum circuit classes

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QCIRCUIT_HPP
#define QCIRCUIT_HPP

#include <QBase.hpp>
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

/** @namespace LibKet::circuits

    @brief
    The LibKet::circuits namespace, containing all circuits of the LibKet
    project

    The LibKet::circuits namespace contains all circuits of the LibKet
    project that is exposed to the end-user. All functionality in
    this namespace has a stable API over future LibKet releases.
 */
namespace circuits {

/**
@brief LibKet quantum circuit base class

The LibKet quantum circuit base class is the base class of all LibKet quantum
circuit classes.
*/
class QCircuit : public QBase
{};

} // namespace circuits

} // namespace LibKet

#endif // QCIRCUIT_HPP
