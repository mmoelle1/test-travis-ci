/** @file libket/gates/QGate_Rotate_X90.hpp

    @brief LibKet quantum Rotate_X90 class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_ROTATE_X90_HPP
#define QGATE_ROTATE_X90_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet Rotate_X90 gate class

The LibKet Rotate_X90 gate class implements the quantum Rotate_X90
gate for an arbitrary number of quantum bits
*/

class QRotate_X90 : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("RX[PI/2.0] q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("cirq.XPowGate(exponent=" + utils::to_string(M_PI_2) +
                         ").on(q[" + utils::to_string(i) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    std::string _expr = "x90 q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("rx(" + utils::to_string(M_PI_2) + ") q[" +
                         utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().rx90(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\tx90 q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("RX(" + utils::to_string(M_PI_2) + ") " +
                         utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    for (auto i : _filter::range(data))
      quest::rotateX(data.reg(), i, M_PI_2);

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::rx(i, M_PI_2));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QRotate_X90& gate)
{
  os << "rx90(";
  return os;
}

/**
@brief LibKet Rotate_X90 gate creator

This overload of the LibKet::gate:rotate_x90() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::rotate_x90();
\endcode
*/
inline constexpr auto
rotate_x90() noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_X90>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

// Forward declaration
class QRotate_MX90;

/**
@brief LibKet Rotate_X90 gate creator

This overload of the LibKet::gates::rotate_x90() function eliminates
the application of the Rotate_X90 gate to a Rotate_MX90 gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
rotate_x90(
  const UnaryQGate<_expr,
                   QRotate_MX90,
                   typename filters::getFilter<_expr>::type>& expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet Rotate_X90 gate creator

This overload of the LibKet::gate:rotate_x90() function eliminates the
application of the Rotate_X90 gate to a Rotate_MX90 gate
*/
template<typename _expr>
inline constexpr auto
rotate_x90(
  UnaryQGate<_expr, QRotate_MX90, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Rotate_X90 gate creator

This overload of the LibKet::gates::rotate_x90() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
rotate_x90(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QRotate_X90,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Rotate_X90 gate creator

This overload of the LibKet::gates::rotate_x90() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
rotate_x90(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QRotate_X90,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Rotate_X90 gate creator

Function alias for LibKet::gates::rotate_x90
*/
template<typename... Args>
inline constexpr auto
ROTATE_X90(Args&&... args)
{
  return rotate_x90(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X90 gate creator

Function alias for LibKet::gates::rotate_x90
*/
template<typename... Args>
inline constexpr auto
rx90(Args&&... args)
{
  return rotate_x90(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X90 gate creator

Function alias for LibKet::gates::rotate_x90
*/
template<typename... Args>
inline constexpr auto
RX90(Args&&... args)
{
  return rotate_x90(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X90 gate creator

Function alias for LibKet::gates::rotate_x90
*/
template<typename... Args>
inline constexpr auto
Rx90(Args&&... args)
{
  return rotate_x90(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X90 gate creator

Function alias for LibKet::gates::rotate_x90
*/
template<typename... Args>
inline constexpr auto
x90(Args&&... args)
{
  return rotate_x90(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X90 gate creator

Function alias for LibKet::gates::rotate_x90
*/
template<typename... Args>
inline constexpr auto
X90(Args&&... args)
{
  return rotate_x90(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QRotate_X90::operator()(const T& t) const noexcept
{
  return rotate_x90(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QRotate_X90::operator()(T&& t) const noexcept
{
  return rotate_x90(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QRotate_X90 objects
*/
template<std::size_t level = 1>
inline static auto
show(const QRotate_X90& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QRotate_X90\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_ROTATE_X90_HPP
