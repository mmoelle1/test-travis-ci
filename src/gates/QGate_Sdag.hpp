/** @file libket/gates/QGate_Sdag.hpp

    @brief LibKet quantum S-dagger gate class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_SDAG_HPP
#define QGATE_SDAG_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
class QS;

/**
@brief LibKet S-dagger gate class

The LibKet S-dagger gate class implements the quantum S-dagger
gate for an arbitrary number of quantum bits
*/

class QSdag : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("DAG(S) q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("(cirq.S**-1).on(q[" + utils::to_string(i) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    std::string _expr = "sdag q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("sdg q[" + utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().sdag(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\tsdag q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("DAGGER S " + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    quest::ComplexMatrix2 sdag = { .real = { { 1.0, 0.0 }, { 0.0, 0.0 } },
                                   .imag = { { 0.0, 0.0 }, { 0.0, -1.0 } } };
    for (auto i : _filter::range(data))
      quest::unitary(data.reg(), i, sdag);

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::s_dag_gate(i));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QSdag& gate)
{
  os << "sdag(";
  return os;
}

/**
@brief LibKet S-dagger gate creator

This overload of the LibKet::gates::gate_sdag() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::sdag();
\endcode
*/
inline constexpr auto
sdag() noexcept
{
  return UnaryQGate<filters::QFilter, QSdag>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES
/**
@brief LibKet S-dagger gate creator

This overload of the LibKet::gates::gate_sdag() function
eliminates the application of the S-dagger gate to its adjoint,
the S gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
sdag(const UnaryQGate<_expr, QS, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet S-dagger gate creator

This overload of the LibKet::gates::gate_sdag() function
eliminates the application of the S-dagger gate to its adjoint,
the S gate
*/
template<typename _expr>
inline constexpr auto
sdag(UnaryQGate<_expr, QS, typename filters::getFilter<_expr>::type>&&
       expr) noexcept
{
  return expr.expr;
}
#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet S-dagger gate creator

This overload of the LibKet::gates::sdag() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
sdag(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QSdag, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet S-dagger gate creator

This overload of the LibKet::gates::sdag() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
sdag(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QSdag, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet S-dagger gate creator

Function alias for LibKet::gates::sdag
*/
template<typename... Args>
inline constexpr auto
SDAG(Args&&... args)
{
  return sdag(std::forward<Args>(args)...);
}

/**
@brief LibKet S-dagger gate creator

Function alias for LibKet::gates::sdag
*/
template<typename... Args>
inline constexpr auto
Sdag(Args&&... args)
{
  return sdag(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QSdag::operator()(const T& t) const noexcept
{
  return sdag(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QSdag::operator()(T&& t) const noexcept
{
  return sdag(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QSdag objects
*/
template<std::size_t level = 1>
inline static auto
show(const QSdag& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QSdag\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_SDAG_HPP
