/** @file libket/gates/QGate_CPhasedag.hpp

    @brief LibKet quantum CPHASEDAG (controlled phase shift) class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_CPHASEDAG_HPP
#define QGATE_CPHASEDAG_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet CPHASEDAG (controlled phase shift) gate class

The LibKet CPHASEDAG (controlled phase shift) gate class implements
the quantum controlled phase shift gate for an arbitrary number of
quantum bits
*/

template<typename _angle, typename _tol = QConst_t(0.0)>
class QCPhasedag : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEDAG gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel("CTRL(PH[" + (-_angle{}).to_string() + "]) q[" +
                           utils::to_string(i) + "],q[" +
                           utils::to_string(*(it++)) + "]\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEDAG gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel(
          "cirq.CZPowGate(exponent=" + (-_angle{}).to_string() + ").on(q[" +
          utils::to_string(i) + "],q[" + utils::to_string(*(it++)) + "])\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEDAG gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "cr q[";
      for (auto i : _filter0::range(data))
        _expr += utils::to_string(i) +
                 (i != *(_filter0::range(data).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(data))
        _expr += utils::to_string(i) +
                 (i != *(_filter1::range(data).end() - 1) ? "," : "], ");
      _expr += (-_angle{}).to_string() + "\n";
      data.append_kernel(_expr);
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEDAG gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel("cu1(" + (-_angle{}).to_string() + ") q[" +
                           utils::to_string(i) + "], q[" +
                           utils::to_string(*(it++)) + "];\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    std::cerr << "The CR/CPHASE gate is not implemented for OpenQL!!!\n";
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEDAG gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel([&]() {
          data.kernel().controlled_rz(i, *(it++), (-_angle{}).value());
        });
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEDAG gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel("\tcphasedag q" + utils::to_string(i) + ",q" +
                           utils::to_string(*(it++)) + " # " +
                           (-_angle{}).to_string() + "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEDAG gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel("CPHASE (" + (-_angle{}).to_string() + ") " +
                           utils::to_string(i) + " " +
                           utils::to_string(*(it++)) + "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEDAG gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        quest::controlledPhaseShift(
          data.reg(), i, *(it++), (qreal)(-_angle{}).value());
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEDAG gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel(
          new qx::ctrl_phase_shift(i, *(it++), (double)(-_angle{}).value()));
    }
    return data;
  }
#endif
};

/// Serialize operator
template<typename _angle, typename _tol = QConst_t(0.0)>
std::ostream&
operator<<(std::ostream& os, const QCPhasedag<_angle, _tol>& gate)
{
  os << "crdag<" << _tol::to_type() << ">(" << _angle::to_obj() << ",";
  return os;
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

This overload of the LibKet::gates::cphasedag() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::cphasedag();
\endcode
*/
template<typename _tol = QConst_t(0.0), typename _angle>
inline constexpr auto cphasedag(_angle) noexcept
{
  return BinaryQGate<filters::QFilter,
                     filters::QFilter,
                     QCPhasedag<_angle, _tol>>(filters::QFilter{},
                                               filters::QFilter{});
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

This overload of the LibKet::gates::cphasedag() function accepts
two expressions as constant reference
*/
template<typename _tol = QConst_t(0.0),
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasedag(_angle, const _expr0& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCPhasedag<_angle, _tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

This overload of the LibKet::gates::cphasedag() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<typename _tol = QConst_t(0.0),
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasedag(_angle, const _expr0& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCPhasedag<_angle, _tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

This overload of the LibKet::gates::cphasedag() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<typename _tol = QConst_t(0.0),
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasedag(_angle, _expr0&& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCPhasedag<_angle, _tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

This overload of the LibKet::gates::cphasedag() function accepts
two expression as universal reference
*/
template<typename _tol = QConst_t(0.0),
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasedag(_angle, _expr0&& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCPhasedag<_angle, _tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

Function alias for LibKet::gates::cphasedag
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
CPHASEDAG(Args&&... args)
{
  return cphasedag<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

Function alias for LibKet::gates::cphasedag
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
CPHASEdag(Args&&... args)
{
  return cphasedag<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

Function alias for LibKet::gates::cphasedag
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
crdag(Args&&... args)
{
  return cphasedag<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

Function alias for LibKet::gates::cphasedag
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
CRDAG(Args&&... args)
{
  return cphasedag<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet CPHASEDAG (inverse controlled phase shift) gate creator

Function alias for LibKet::gates::cphasedag
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
CRdag(Args&&... args)
{
  return cphasedag<_tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename _angle, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhasedag<_angle, _tol>::operator()(const T0& t0, const T1& t1) const noexcept
{
  return cphasedag<_tol>(_angle{}, std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by constant and universal reference
template<typename _angle, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhasedag<_angle, _tol>::operator()(const T0& t0, T1&& t1) const noexcept
{
  return cphasedag<_tol>(_angle{}, std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename _angle, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhasedag<_angle, _tol>::operator()(T0&& t0, const T1& t1) const noexcept
{
  return cphasedag<_tol>(_angle{}, std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<typename _angle, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhasedag<_angle, _tol>::operator()(T0&& t0, T1&& t1) const noexcept
{
  return cphasedag<_tol>(_angle{}, std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for QCPhasedag objects
*/
template<std::size_t level = 1, typename _angle, typename _tol>
inline static auto
show(const QCPhasedag<_angle, _tol>& gate,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QCPhasedag " << _angle::to_string() << "\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CPHASEDAG_HPP
