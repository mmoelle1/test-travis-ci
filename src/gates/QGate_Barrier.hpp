/** @file libket/gates/QGate_Barrier.hpp

    @brief LibKet quantum synchronization gate class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_BARRIER_HPP
#define QGATE_BARRIER_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet quantum synchronization gate class

The quantum synchronization gate ensures that quantum expression
optimization does not proceed beyond the synchronization barrier.

The following code creates a quantum expression consisting of
three hadamard gates applied consecutively, which by the built-in
optimization of quantum expressions yields a single Hadamard gate

\code
auto expr = gates::hadamard( gates::hadamard( gates::hadamard() ) ); // same as
autoexpr = gates::hadamard(); \endcode

To prevent the built-in optimization from taking place, the
quantum synchronization gate can be inserted between the gates

\code
auto expr = gates::hadamard( gates::barrier ( gates::hadamard( gates::barrier (
gates::hadamard() ) ) ) ); \endcode
*/
class QBarrier : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    data.append_kernel("barrier q;");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    data.append_kernel([&]() {
      data.kernel().wait(std::vector<std::size_t>(_filter::range(data)), 0);
    });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    // QuEST does not support waiting
    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    // for (auto i : _filter::range(data))
    // data.append_kernel(new qx::wait(i));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QBarrier& gate)
{
  os << "barrier(";
  return os;
}

/**
@brief LibKet quantum synchronization gate creator

This overload of the LibKet::gates::barrier() function can be used
as terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr1 = gates::hadamard( gates::barrier() );
\endcode

If the so-defined expression is used as sub-expression then the
synchronization gate ensures that no optimization takes place

\code
auto expr2 = expr1( gates::hadamard() );
\endcode
*/
inline constexpr auto
barrier() noexcept
{
  return UnaryQGate<filters::QFilter, QBarrier, filters::QFilter>(
    filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Barrier gate creator

This overload of the LibKet::gates::barrier() function eliminates the
double-application of the Barrier gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
barrier(
  const UnaryQGate<_expr, QBarrier, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr;
}

/**
@brief LibKet Barrier gate creator

This overload of the LibKet::gates::barrier() function eliminates the
double-application of the Barrier gate
*/
template<typename _expr>
inline constexpr auto
barrier(UnaryQGate<_expr, QBarrier, typename filters::getFilter<_expr>::type>&&
          expr) noexcept
{
  return expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet quantum synchronization gate creator

This overload of the LibKet::gates::barrier() function accepts an
expression as constant reference
*/
template<typename _expr>
inline constexpr auto
barrier(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QBarrier, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet quantum synchronization gate creator

This overload of the LibKet::gates::barrier() function accepts an
expression as universal reference
*/
template<typename _expr>
inline constexpr auto
barrier(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QBarrier, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet quantum synchronization gate creator

Function alias for LibKet::gates::barrier
*/
template<typename... Args>
inline constexpr auto
BARRIER(Args&&... args)
{
  return barrier(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QBarrier::operator()(const T& t) const noexcept
{
  return barrier(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QBarrier::operator()(T&& t) const noexcept
{
  return barrier(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QBarrier objects
*/
template<std::size_t level = 1>
inline static auto
show(const QBarrier& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QBarrier\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_BARRIER_HPP
