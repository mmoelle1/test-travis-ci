/** @file libket/gates/QGate_Swap.hpp

    @brief LibKet quantum swap class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_SWAP_HPP
#define QGATE_SWAP_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet swap gate class

The LibKet swap gate class implements the quantum swap
gate for an arbitrary number of quantum bits
*/

class QSwap : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("SWAP q[" + utils::to_string(i) + "],q[" +
                         utils::to_string(*(it++)) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("cirq.SWAP.on(q[" + utils::to_string(i) + "],q[" +
                         utils::to_string(*(it++)) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    std::string _expr = "swap q[";
    for (auto i : _filter0::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter0::range(data).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter1::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("swap q[" + utils::to_string(i) + "], q[" +
                         utils::to_string(*(it++)) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel([&]() { data.kernel().swap(i, *(it++)); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("\tswap q" + utils::to_string(i) + ",q" +
                         utils::to_string(*(it++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("SWAP " + utils::to_string(i) + " " +
                         utils::to_string(*(it++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      quest::swapGate(data.reg(), i, *(it++));

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel(new qx::swap(i, *(it++)));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QSwap& gate)
{
  os << "swap(";
  return os;
}

/**
@brief LibKet swap gate creator

This overload of the LibKet::gate:swap() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::swap();
\endcode
*/
inline constexpr auto
swap() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QSwap>(
    filters::QFilter{}, filters::QFilter{});
}

namespace detail {
template<typename _expr0, typename _expr1, typename = void>
struct swap_impl
{
  inline static constexpr auto apply(const _expr0& expr0, const _expr1& expr1)
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       QSwap,
                       decltype(typename filters::getFilter<_expr0>::type{} <<
                                typename filters::getFilter<_expr1>::type{})>(
      expr0, expr1);
  }
};
} // namespace detail

#ifdef LIBKET_OPTIMIZE_GATES

namespace detail {

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0_0,
         typename __expr0_1,
         typename __filter0,
         typename __expr1_0,
         typename __expr1_1,
         typename __filter1>
struct swap_impl<
  BinaryQGate<__expr0_0, __expr0_1, QSwap, __filter0>,
  BinaryQGate<__expr1_0, __expr1_1, QSwap, __filter1>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_0>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_1>::type>::value) ||
     (std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_1>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_0>::type>::value))>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0_0, __expr0_1, QSwap, __filter0>& expr0,
    const BinaryQGate<__expr1_0, __expr1_1, QSwap, __filter1>& expr1)
  {
#ifdef LIBKET_L2R_EVALUATION
    return swap_impl<decltype(typename filters::getFilter<__filter0>::type{}(
                       all(expr1.expr1(all(
                         expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))),
                     typename filters::getFilter<__filter1>::type>::
      apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(
              all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))),
            typename filters::getFilter<__filter1>::type{});
#else
    return swap_impl<typename filters::getFilter<__filter0>::type,
                     decltype(typename filters::getFilter<__filter1>::type{}(
                       all(expr0.expr0(all(
                         expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>::
      apply(typename filters::getFilter<__filter0>::type{},
            typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(
              all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))));
#endif
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{});
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr0)));
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr1)));
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr1.expr1(all(expr1.expr0)))
#else
      all(expr1.expr0(all(expr1.expr1)))
#endif
    );
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{});
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr0)));
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr1)));
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr0.expr1(all(expr0.expr0)))
#else
      all(expr0.expr0(all(expr0.expr1)))
#endif
    );
  }
};

#ifdef LIBKET_L2R_EVALUATION

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QSwap here */
    !std::is_same<typename gates::getGate<_expr0>::type, QSwap>::value>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(all(expr0)));
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr0(all(expr0)))));
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Swap gate is handled explicitly */
    (!std::is_base_of<
      QSwap,
      typename std::template decay<_expr0>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr1(all(expr0)))));
  }
};

#else // not LIBKET_L2R_EVALUATION

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QSwap here */
    !std::is_same<typename gates::getGate<_expr1>::type, QSwap>::value>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr1)));
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr0(all(expr1)))));
  }
};

/**
   @brief LibKet Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Swap gate is handled explicitly */
    (!std::is_base_of<
      QSwap,
      typename std::template decay<_expr1>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr1(all(expr1)))));
  }
};

#endif // LIBKET_L2R_EVALUATION

} // namespace details

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet swap gate creator

This overload of the LibKet::gates::swap() function accepts
two expressions as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
swap(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return detail::swap_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet swap gate creator

This overload of the LibKet::gates::swap() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
swap(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return detail::swap_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet swap gate creator

This overload of the LibKet::gates::swap() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
swap(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return detail::swap_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet swap gate creator

This overload of the LibKet::gates::swap() function accepts
two expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
swap(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return detail::swap_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet swap gate creator

Function alias for LibKet::gates::swap
*/
template<typename... Args>
inline constexpr auto
SWAP(Args&&... args)
{
  return swap(std::forward<Args>(args)...);
}

/**
@brief LibKet swap gate creator

Function alias for LibKet::gates::swap
*/
template<typename... Args>
inline constexpr auto
swapdag(Args&&... args)
{
  return swap(std::forward<Args>(args)...);
}

/**
@brief LibKet swap gate creator

Function alias for LibKet::gates::swap
*/
template<typename... Args>
inline constexpr auto
SWAPdag(Args&&... args)
{
  return swap(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T0, typename T1>
inline constexpr auto
QSwap::operator()(const T0& t0, const T1& t1) const noexcept
{
  return swap(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1>
inline constexpr auto
QSwap::operator()(const T0& t0, T1&& t1) const noexcept
{
  return swap(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename T0, typename T1>
inline constexpr auto
QSwap::operator()(T0&& t0, const T1& t1) const noexcept
{
  return swap(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<typename T0, typename T1>
inline constexpr auto
QSwap::operator()(T0&& t0, T1&& t1) const noexcept
{
  return swap(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for QSwap objects
*/
template<std::size_t level = 1>
inline static auto
show(const QSwap& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QSwap\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_SWAP_HPP
