/** @file libket/gates/QGate_CCNOT.hpp

    @brief LibKet quantum CCNOT (Toffoli gate) class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_CCNOT_HPP
#define QGATE_CCNOT_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet CCNOT (Toffoli) gate class

The LibKet CCNOT (Toffoli) gate class implements the quantum
CCNOT (Toffoli) gate for an arbitrary number of quantum bits
*/

class QCCNOT : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(const T0& t0,
                                   const T1& t1,
                                   const T2& t2) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(const T0& t0,
                                   const T1& t1,
                                   T2&& t2) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(const T0& t0,
                                   T1&& t1,
                                   const T2& t2) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(const T0& t0,
                                   T1&& t1,
                                   T2&& t2) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(T0&& t0,
                                   const T1& t1,
                                   const T2& t2) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(T0&& t0,
                                   const T1& t1,
                                   T2&& t2) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(T0&& t0,
                                   T1&& t1,
                                   const T2& t2) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(T0&& t0, T1&& t1, T2&& t2) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "CCNOT (Toffoli) gate can only be applied to quantum objects of the "
      "same size");
    auto it1 = _filter1::range(data).begin();
    auto it2 = _filter2::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("CCNOT q[" + utils::to_string(i) + "],q[" +
                         utils::to_string(*(it1++)) + "],q[" +
                         utils::to_string(*(it2++)) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "CCNOT (Toffoli) gate can only be applied to quantum objects of the "
      "same size");
    auto it1 = _filter1::range(data).begin();
    auto it2 = _filter2::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("cirq.CCNOT.on(q[" + utils::to_string(i) + "],q[" +
                         utils::to_string(*(it1++)) + "],q[" +
                         utils::to_string(*(it2++)) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "CCNOT (Toffoli) gate can only be applied to quantum objects of the "
      "same size");
    std::string _expr = "toffoli q[";
    for (auto i : _filter0::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter0::range(data).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter1::range(data).end() - 1) ? "," : "], q[");
    for (auto i : _filter2::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter2::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "CCNOT (Toffoli) gate can only be applied to quantum objects of the "
      "same size");
    auto it1 = _filter1::range(data).begin();
    auto it2 = _filter2::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("ccx q[" + utils::to_string(i) + "], q[" +
                         utils::to_string(*(it1++)) + "], q[" +
                         utils::to_string(*(it2++)) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "CCNOT (Toffoli) gate can only be applied to quantum objects of the "
      "same size");
    auto it1 = _filter1::range(data).begin();
    auto it2 = _filter2::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel(
        [&]() { data.kernel().toffoli(i, *(it1++), *(it2++)); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "CCNOT gate can only be applied to quantum objects of the same size");
    auto it1 = _filter1::range(data).begin();
    auto it2 = _filter2::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("\ttoffoli q" + utils::to_string(i) + ",q" +
                         utils::to_string(*(it1++)) + ",q" +
                         utils::to_string(*(it2++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "CCNOT (Toffoli) gate can only be applied to quantum objects of the "
      "same size");
    auto it1 = _filter1::range(data).begin();
    auto it2 = _filter2::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("CCNOT " + utils::to_string(i) + " " +
                         utils::to_string(*(it1++)) + " " +
                         utils::to_string(*(it2++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "CCNOT (Toffoli) gate can only be applied to quantum objects of the "
      "same size");
    auto it1 = _filter1::range(data).begin();
    auto it2 = _filter2::range(data).begin();
    for (auto i : _filter0::range(data)) {
      int ctrl[] = { (int)*(it1++), (int)*(it2++) };
      quest::multiControlledUnitary(
        data.reg(), ctrl, 2, i, quest::ComplexMatrix2({ 0, 1, 1, 0 }));
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "CCNOT (Toffoli) gate can only be applied to quantum objects of the "
      "same size");
    auto it1 = _filter1::range(data).begin();
    auto it2 = _filter2::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel(new qx::toffoli(i, *(it1++), *(it2++)));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QCCNOT& gate)
{
  os << "ccnot(";
  return os;
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::ccnot();
\endcode
*/
inline constexpr auto
ccnot() noexcept
{
  return TernaryQGate<filters::QFilter,
                      filters::QFilter,
                      filters::QFilter,
                      QCCNOT>(
    filters::QFilter{}, filters::QFilter{}, filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function
eliminates the double-application of the CCNOT gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
ccnot(const UnaryQGate<_expr, QCCNOT, typename filters::getFilter<_expr>::type>&
        expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function
eliminates the double-application of the CCNOT gate
*/
template<typename _expr>
inline constexpr auto
ccnot(UnaryQGate<_expr, QCCNOT, typename filters::getFilter<_expr>::type>&&
        expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function accepts
three expressions as constant reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
ccnot(const _expr0& expr0, const _expr1& expr1, const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCCNOT,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function accepts the
first and second expression as constant reference, and the
third expression as universal reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
ccnot(const _expr0& expr0, const _expr1& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCCNOT,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function accepts the
first expression as constant reference, the second expression
as universal reference, and the third expression as constant
reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
ccnot(const _expr0& expr0, _expr1&& expr1, const _expr2 expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCCNOT,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function accepts the
first expression as constant reference and the second and third
expression as universal reference

*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
ccnot(const _expr0& expr0, _expr1&& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCCNOT,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function accepts the
first expression as universal reference and the second and
third expression as constant reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
ccnot(_expr0&& expr0, const _expr1& expr1, const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCCNOT,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function accepts the
first expression as universal reference, the second expression
as constant reference, and the third expression as universal
reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
ccnot(_expr0&& expr0, const _expr1& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCCNOT,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function accepts the
first and second expression as universal reference, and the
third expression as constant reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
ccnot(_expr0&& expr0, _expr1&& expr1, const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCCNOT,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

This overload of the LibKet::gates::ccnot() function accepts all
three expressions as universal reference

*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
ccnot(_expr0&& expr0, _expr1&& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCCNOT,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

Function alias for LibKet::gates::ccnot
*/
template<typename... Args>
inline constexpr auto
CCNOT(Args&&... args)
{
  return ccnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

Function alias for LibKet::gates::ccnot
*/
template<typename... Args>
inline constexpr auto
toffoli(Args&&... args)
{
  return ccnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

Function alias for LibKet::gates::ccnot
*/
template<typename... Args>
inline constexpr auto
TOFFOLI(Args&&... args)
{
  return ccnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

Function alias for LibKet::gates::ccnot
*/
template<typename... Args>
inline constexpr auto
ccnotdag(Args&&... args)
{
  return ccnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

Function alias for LibKet::gates::ccnot
*/
template<typename... Args>
inline constexpr auto
CCNOTdag(Args&&... args)
{
  return ccnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

Function alias for LibKet::gates::ccnot
*/
template<typename... Args>
inline constexpr auto
toffolidag(Args&&... args)
{
  return ccnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CCNOT (Toffoli) gate creator

Function alias for LibKet::gates::ccnot
*/
template<typename... Args>
inline constexpr auto
TOFFOLIdag(Args&&... args)
{
  return ccnot(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCCNOT::operator()(const T0& t0, const T1& t1, const T2& t2) const noexcept
{
  return ccnot(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCCNOT::operator()(const T0& t0, const T1& t1, T2&& t2) const noexcept
{
  return ccnot(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCCNOT::operator()(const T0& t0, T1&& t1, const T2& t2) const noexcept
{
  return ccnot(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCCNOT::operator()(const T0& t0, T1&& t1, T2&& t2) const noexcept
{
  return ccnot(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCCNOT::operator()(T0&& t0, const T1& t1, const T2& t2) const noexcept
{
  return ccnot(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCCNOT::operator()(T0&& t0, const T1& t1, T2&& t2) const noexcept
{
  return ccnot(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCCNOT::operator()(T0&& t0, T1&& t1, const T2& t2) const noexcept
{
  return ccnot(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}

/// Operator() - by universal reference
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCCNOT::operator()(T0&& t0, T1&& t1, T2&& t2) const noexcept
{
  return ccnot(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}

/**
   @brief LibKet show gate type - specialization for QCCNOT objects
*/
template<std::size_t level = 1>
inline static auto
show(const QCCNOT& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QCCNOT\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CCNOT_HPP
