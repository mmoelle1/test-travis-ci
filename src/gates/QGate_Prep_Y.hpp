/** @file libket/gates/QGate_Prep_Y.hpp

    @brief LibKet quantum Prep_Y class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_PREP_Y_HPP
#define QGATE_PREP_Y_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet Prep_Y class

The LibKet Prep_Y class implements the initialization of an arbitrary
number of quantum bits in the Y-basis. Striktly speaking, prep_y is
not a quantum gate.
*/

class QPrep_Y : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("RESET q[" + utils::to_string(i) + "]\n");
      data.append_kernel("S q[" + utils::to_string(i) + "]\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("cirq.reset(q[" + utils::to_string(i) + "])\n");
      data.append_kernel("cirq.S.on(q[" + utils::to_string(i) + "])\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    std::string _expr = "prep_y q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("reset q[" + utils::to_string(i) + "];\n");
      data.append_kernel("s q[" + utils::to_string(i) + "];\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() {
        data.kernel().prepz(i);
        data.kernel().s(i);
      });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\tprep_y q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("RESET " + utils::to_string(i) + "\n");
      data.append_kernel("S " + utils::to_string(i) + "\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    quest::initZeroState(data.reg());

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::prepy(i));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QPrep_Y& gate)
{
  os << "prep_y(";
  return os;
}

/**
@brief LibKet Prep_Y gate creator

This overload of the LibKet::gates::prep_y() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::prep_y();
\endcode
*/
inline constexpr auto
prep_y() noexcept
{
  return UnaryQGate<filters::QFilter, QPrep_Y>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Prep_Y gate creator

This overload of the LibKet::gates::prep_y() function eliminates the
double-application of the Prep_Y gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
prep_y(
  const UnaryQGate<_expr, QPrep_Y, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr;
}

/**
@brief LibKet Prep_Y gate creator

This overload of the LibKet::gates::prep_y() function eliminates the
double-application of the Prep_Y gate
*/
template<typename _expr>
inline constexpr auto
prep_y(UnaryQGate<_expr, QPrep_Y, typename filters::getFilter<_expr>::type>&&
         expr) noexcept
{
  return expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Prep_Y gate creator

This overload of the LibKet::gates::prep_y() function accepts an
expression as constant reference
*/
template<typename _expr>
inline constexpr auto
prep_y(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QPrep_Y, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet Prep_Y gate creator

This overload of the LibKet::gates::prep_y() function accepts an
expression as universal reference
*/
template<typename _expr>
inline constexpr auto
prep_y(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QPrep_Y, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet Prep_Y gate creator

Function alias for LibKet::gates::prep_y
*/
template<typename... Args>
inline constexpr auto
PREP_Y(Args&&... args)
{
  return prep_y(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QPrep_Y::operator()(const T& t) const noexcept
{
  return prep_y(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QPrep_Y::operator()(T&& t) const noexcept
{
  return prep_y(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QPrep_Y objects
*/
template<std::size_t level = 1>
inline static auto
show(const QPrep_Y& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QPrep_Y\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_PREP_Y_HPP
