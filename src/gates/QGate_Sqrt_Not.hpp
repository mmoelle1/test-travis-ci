/** @file libket/gates/QGate_Sqrt_Not.hpp

    @brief LibKet quantum square-root-of-not class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_SQRT_NOT_HPP
#define QGATE_SQRT_NOT_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>
#include <gates/QGate_Rotate_Y.hpp>
#include <gates/QGate_Rotate_Z.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace gates {

/**
@brief LibKet square-root-of-not gate class

The LibKet quantum square-root-of-not gate class implements
the square-root-of-not gate for an arbitrary number of qubits
*/
template<typename _tol = QConst_t(0.0)>
class QSqrt_Not : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

  /// Apply function - used for all backends
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  inline static QData<_qubits, _qbackend>& apply(
    QData<_qubits, _qbackend>& data) noexcept
  {
    auto expr = Rz(-QConst_M_PI_2, // QConst(-1.5708),
                   Ry(QConst_M_PI_2, Rz(QConst_M_PI_2, tag<0>(_filter{}))));
    return expr(data);
  }
};

/// Serialize operator
template<typename _tol = QConst_t(0.0)>
std::ostream&
operator<<(std::ostream& os, const QSqrt_Not<_tol>& gate)
{
  os << "sqrt_not<" << _tol::to_type() << ">(";
  return os;
}

/**
@brief LibKet square-root-of-not gate creator

This overload of the LibKet::gates::sqrt_not() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = gates::sqrt_not();
\endcode
*/
template<typename _tol = QConst_t(0.0)>
inline constexpr auto
sqrt_not() noexcept
{
  return UnaryQGate<filters::QFilter, QSqrt_Not<_tol>>(filters::QFilter{});
}

/**
@brief LibKet square-root-of-not gate creator

This overload of the LibKet::gates::sqrt_not() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = gates::sqrt_not(expr);
\endcode
*/
template<typename _tol = QConst_t(0.0), typename _expr>
inline constexpr auto
sqrt_not(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QSqrt_Not<_tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet square-root-of-not gate creator

This overload of the LibKet::gates::sqrt_not() function accepts
an expression as universal reference
*/
template<typename _tol = QConst_t(0.0), typename _expr>
inline constexpr auto
sqrt_not(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QSqrt_Not<_tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet square-root-of-not gate creator

Function alias for LibKet::gates::sqrt_not()
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
sqrt_not(Args&&... args)
{
  return sqrt_not<_tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename _tol>
template<typename T>
inline constexpr auto
QSqrt_Not<_tol>::operator()(const T& t) const noexcept
{
  return sqrt_not<_tol>(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename _tol>
template<typename T>
inline constexpr auto
QSqrt_Not<_tol>::operator()(T&& t) const noexcept
{
  return sqrt_not<_tol>(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for square-root-of-not objects
*/
template<std::size_t level = 1, typename _tol = QConst_t(0.0)>
inline static auto
show(const QSqrt_Not<_tol>& gate,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "Sqrt_Not\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_SQRT_NOT_HPP
