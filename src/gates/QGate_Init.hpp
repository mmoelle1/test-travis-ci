/** @file libket/gates/QGate_Init.hpp

    @brief LibKet quantum initialization gate class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_INIT_HPP
#define QGATE_INIT_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet quantum initialization gate class

The LibKet quantum initialization gate class implements a
syncronization barrier gate
*/
class QInit : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    data.append_kernel("BEGIN\n");
    data.append_kernel("qubits " + utils::to_string(_qubits) + "\n");
    data.append_kernel("cbits " + utils::to_string(_qubits) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    data.append_kernel("version 1.0\n");
    data.append_kernel("qubits " + utils::to_string(_qubits) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    data.append_kernel("OPENQASM 2.0;\n");
    data.append_kernel("include \"qelib1.inc\";\n");
    data.append_kernel("qreg q[" + utils::to_string(_qubits) + "];\n");
    data.append_kernel("creg c[" + utils::to_string(_qubits) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    data.append_kernel("\tdef cphase,1,'R_{\\theta}'\n");
    data.append_kernel("\tdef cphasedag,1,'R_{\\theta}'\n");
    data.append_kernel("\tdef cy,1,'CY'\n");
    data.append_kernel("\tdef cz,1,'CZ'\n");
    data.append_kernel("\tdef i,0,'I'\n");
    data.append_kernel("\tdef prep_x,0,'Prep_x'\n");
    data.append_kernel("\tdef prep_y,0,'Prep_y'\n");
    data.append_kernel("\tdef prep_z,0,'Prep_z'\n");
    data.append_kernel("\tdef mx90,0,'mX_{90}'\n");
    data.append_kernel("\tdef my90,0,'mY_{90}'\n");
    data.append_kernel("\tdef reset,0,'Reset'\n");
    data.append_kernel("\tdef rx,0,'R_x'\n");
    data.append_kernel("\tdef rxdag,0,'R_x^\\dagger'\n");
    data.append_kernel("\tdef ry,0,'R_y'\n");
    data.append_kernel("\tdef rydag,0,'R_y^\\dagger'\n");
    data.append_kernel("\tdef rz,0,'R_z'\n");
    data.append_kernel("\tdef rzdag,0,'R_z^\\dagger'\n");
    data.append_kernel("\tdef s,0,'S'\n");
    data.append_kernel("\tdef sdag,0,'S^\\dagger'\n");
    data.append_kernel("\tdef t,0,'T'\n");
    data.append_kernel("\tdef tdag,0,'T^\\dagger'\n");
    data.append_kernel("\tdef x,0,'X'\n");
    data.append_kernel("\tdef x90,0,'X_{90}'\n");
    data.append_kernel("\tdef y,0,'Y'\n");
    data.append_kernel("\tdef y90,0,'Y_{90}'\n");
    data.append_kernel("\tdef z,0,'Z'\n");

    for (std::size_t k = 0; k < 128; k++)
      data.append_kernel("\tdef cphase" + utils::to_string(k) +
                         ",1,'R_{\\2 pi i/2^" + utils::to_string(k) + "}'\n");

    for (std::size_t i = 0; i < _qubits; ++i)
      data.append_kernel("\tqubit q" + utils::to_string(i) + ";0\n");

    for (std::size_t i = 0; i < _qubits; ++i)
      data.append_kernel("\tcbit c" + utils::to_string(i) + ";0\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    data.append_kernel("DECLARE ro BIT[" + utils::to_string(_qubits) + "]\n");
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QUEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    quest::initZeroState(data.reg());
    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QInit& gate)
{
  os << "init(";
  return os;
}

/**
@brief LibKet quantum initialization gate creator

This overload of the LibKet::gates::init() function can be used
as the inner-most gate in a quantum expression

\warning

Be careful with using the initialization gate as terminal in
quantum expressions that serve as sub-expressions. The
following code creates two sub-expressions both using the
initialization gate as terminal and creates them to another
expression. The so-defined quantum expression is likely invalid
since it re-initializes the quantum data within the algorithm

\code
auto expr1 = gates::hadamard( gates::init() );
auto expr2 = gates::hadamard( gates::init() );

auto expr3 = expr1( expr2 );
\endcode

*/
inline constexpr auto
init() noexcept
{
  return UnaryQGate<filters::QFilter, QInit, filters::QFilterSelectAll>(
    filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Init gate creator

This overload of the LibKet::gates::init() function
eliminates the double-application of the Init gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
init(const UnaryQGate<_expr, QInit, _filter>& expr) noexcept
{
  return expr;
}

/**
@brief LibKet Init gate creator

This overload of the LibKet::gates::init() function
eliminates the double-application of the Init gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
init(UnaryQGate<_expr, QInit, _filter>&& expr) noexcept
{
  return expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet quantum initialization gate creator

This overload of the LibKet::gates::init() function accepts an
expression as constant reference
*/
template<typename _expr>
inline constexpr auto
init(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QInit, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet quantum initialization gate creator

This overload of the LibKet::gates::init() function accepts an
expression as constant reference
*/
template<typename _expr>
inline constexpr auto
init(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QInit, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet quantum initialization gate creator

Function alias for LibKet::gates::init
*/
template<typename... Args>
inline constexpr auto
INIT(Args&&... args)
{
  return init(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QInit::operator()(const T& t) const noexcept
{
  return init(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QInit::operator()(T&& t) const noexcept
{
  return init(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QInit objects
*/
template<std::size_t level = 1>
inline static auto
show(const QInit& gate,
     std::ostream& os = std::cout,
     const std::string& prefix = "")
{
  os << "QInit\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_INIT_HPP
